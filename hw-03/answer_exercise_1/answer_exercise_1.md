Para realizar este ejercicio tenemos que crear 3 ojbjetos:
	-Objeto de tipo dployment que contenga 3 replicas, que los pods se expongan en el puerto 80, que los pods usen la version 1.19.4 de nginx y 20 milicores en CPU y 128Mi tanto en los limites como en las requests. En este caso estaría cumpliendo el QoS Guaranteed. (Adjunto: deployment.yml)
	-Objeto de tipo service donde se expone el servicio en el puerto 80. (Adjunto: service.yml)
	-Objeto ingress dónde se especifica que el host sea enrique.student.lasalle.com y que use el servicio anterior en el puerto 80. (Adjunto: ingress.yml)

Antes de ejecutar los procesos debemos habilitar el addons de minikube ingress usando el comando: minikube addons enable ingress. (Adjunto: img1.png y img2.png)

Para realizar este ejercició usaremos el namespace kube-sytem: kubectl config set-context --current --namespace=kube-system

Una vez definidos los objetos procedemos a crearlos. Para ello usaremos el comando:  kubectl apply -f deployment.yml -f service.yml  -f ingress.yml (Adjunto: img3.png)
	
A continuación comprobaremos que el service funciona correctamente y tenemos acceso al nginx. Para ello usaremos el comando: minikube service "svc" --url (Adjunto: img4.png y img5.png)

Cuando hayamos comprobado el funcionamoiento del servicio, ahora procederemos a la configuración de ingress.

Primero consultamos la IP asignada a al ingress creado usando el comando kubectl get ingress. (Adjunto: img6.png) Como se puede observar en la imagen la IP es 192.168.49.2.

El siguiente paso es añadir en el fichero de hosts la IP y la url requerida. (Adjunto img7.png) En windows este fichero se encuentra en: C:/Windows/System32/drivers/etc/

Una vez hecho estos pasos, ya debería funcionar, pero como se puede observar en la imagen (Adjunto img8.png) no he obtenido resultado. He mirado de cambiar la IP en el fichero de hosts por 127.0.0.1 y ejecturar minikube tunnel, tal y como dice al arrancar minikube (Adjunto img9.png)
Pero lo único que he conseguido es cambiar el resultado del curl de time out a connection refused.

Para el apartado dos debemos crear un ceritifaco https, para ello usaremos el comando: openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout tls.key -out tls.crt -subj "/CN=enrique.student.laslle.com"

También crearemos un objeto de tipo secret para guardar el certificado generado. (Adjunto: secret.yml)

NOTA: como se puede observar no se ha conseguido guardar-lo.
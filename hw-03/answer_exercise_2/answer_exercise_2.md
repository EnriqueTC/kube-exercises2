Para realizar este ejercicio he tenido que crear un objeto de tipo StatefulSet, con la imagen de MongoDB y un volumen compartido entre los pods que se creen, en este caso 3.

Para definir un volumen compartido se ha creado un PersistentVolumeClaim y un PersistentVolume dónde usarán un storageClassName predefinido y para ello tambñien se ha creado un objeto de tipo StorageClass.


Para ello ejecutaremos el yaml adjunto llamado statefulSet.yml mediante el comando: kubectl apply -f statefulSet.yml

Una vez ejecutado, podemos entrar en cada pod para realizar una acción al volumen creado y así ver como también se aplica a los demás pods.

Como se puede observar en la imagen adjuntada (Adjunto: img1.png) primero accedemos al pod con indice 0, stateful-0 y entramos en el volume y añadimos el archivo share-doc.txt

A continuación entraremos en los otros dos pods: stateful-1 y stateful-2, y comprobaremos que el documento creado en el Pod anterior también está.

-- Diferencias que existiría si el montaje se hubiera realizado con el objeto de ReplicaSet

	La principal diferéncia si lo montaramos con ReplicaSet, es que este último es stateless, es decir, los pods no tienen estado en canvio, statefulset si tiene estado y es por eso que tiene asignado un identificardor númerico incremental en cada pod que se crea.

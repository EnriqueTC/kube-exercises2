Para la realización de este ejercició he tenido un problema que no me impide la finalicación del ejercicio.

El problema que se muestra en la imagen adjuntada img1.png, se puede observar que al hacer un describe del hpa muestra un error con las métricas.

Para solucionarlo he probado varias cosas como: activar el addons metrics-server de minikube, instalar las métricas en el apartado 'installation' de metrics-server en git (adjunto enlace: https://github.com/kubernetes-sigs/metrics-server#deployment), he probado de cambiar la verisón del HPA a la v1, diferentes tutoriales incluido el de la documentación de Kubernetes, e inifintas búsquedas por foros para mirar de corregir el error pero sin ningún resultado.

Es por ello que no puedo mostrar ninguna captura del resultado obtenido, pero intentaré explicar en palabras y comandos los pasos que seguiria.

1- Primero de todo definimos un yaml con la conficugración de un deployment y de un service. Al yaml de tipo deployment difiniremos las templates de los contenedores con una imagen de nginx y el tag de 1.19.6-alpine y el service lo configuraremos en el puerto 80.
	(Adjunto: deployment.yml)

2- Seguidamente definiremos el objeto de tipo HorizontalPodAutoscaler, este es el HPA, el que se encargará de escalar los pods según los parametros confiugurados. (Adjunto: hpa.yml) 
	Como se puede observar en el adjunto, se ha configurado la CPU al 50% y el mínimo de replicas a 1 y el máximo a 5.

3- Una vez configurados los yaml toca arrancarlos. Para ello usaremos el comando: kubectl apply -f deployment.yml -f hpa.yml

4- El siguiente paso es comprobar que el hpa esté ligado correctamente al deployment. Para ello usaremos el comando: kubectl get hpa (Adjunto: img2.png) Como se puede pbservar en la imgen en 'Targets' dónde pone <unknown> debería salir 0%.

5- A continuación ejecutaremos el comando facilitado en el ejercicio para realizar una prueba de estrés. Al cabo de un momento, al volver a hacer kubectl get hpa, nos debería salir el porcentage aumentado, es decir, en vez de 0% otro valor mayor.

6- La consecuéncia de este incremento indica un amento de los pods si se rebasa el límite de 50%. Supongamos que de 0% ha pasado al 75%, en este caso se duplicaría el número de Pods y pasaríamos de 1 a 2. Si el porcentage sigue aumentando, por ejemplo al 130% el número de Pods sería 3.
	Este aumento en el número de Pods es porqué hemos especificado que un el dployment lanze un Pod y el HPA para ese deployment sea del 50% de CPU, es decir, por cada 50% que aumente la CPU, se creará un nuevo Pod. Si inicialmente el deployment lo configuramos a 2 replicas, por cada aumento del 50% de CPU se añadirian 2 Pods más.